# Conf

## sudo

    # apt install sudo
    # usermod -aG sudo user
    # reboot

## Shell

### oh my bash

https://ohmybash.nntoan.com/

Can change directory path before install (not tested yet)

    export OSH="$HOME/.config/oh-my-bash"

### deps (arch names)

    lazygit ripgrep fd sshfs chafa xclip nodejs npm ttf-0xproto-ner neovim alacritty zellij yazi docker

### config

#### tldr

Add all config:

    cat $PWD/dot_bashrc >> $HOME/.bashrc

#### vi style

    echo "set -o vi" >> $HOME/.bashrc
    echo "bind -f $HOME/.inputrc" >> $HOME/.bashrc

#### conf bin

    mkdir -m $HOME/bin
    echo "PATH=$PATH:$HOME/bin" >> $HOME/.bashrc

#### alias

    echo "source $HOME/.bash_aliases" >> $HOME/.bashrc

#### neovim

    echo "export EDITOR=/usr/bin/nvim" >> $HOME/.bashrc

##### as manpager

    echo "export MANPAGER='nvim +Man!'" >> $HOME/.bashrc

### nerd font hack

https://github.com/ryanoasis/nerd-fonts

Then set the fonts in your terminal emulator (Konsole, Console (gnome-tweak !), ...)

### alacritty

Set link before clone theme

    git clone https://github.com/catppuccin/alacritty.git $HOME/.config/alacritty/catppuccin

### zellij

https://zellij.dev

### yazi

https://yazi-rs.github.io/

## ssh

### client

Create key:

    ssh-keygen -t ed25519

Send key to server:

    ssh-copy-id login@host

#### config

File: $HOME/.ssh/config

    Host * # for all Host
        \# PreferredAuthentications publickey
        Protocol 2
        Compression yes
        
    Host nameAsYouWant
        Hostname 192.168.0.42 # could be dns
        User login

#### sshfs

https://github.com/libfuse/sshfs

### server

settings to add (in `/etc/ssh/sshd_config`):

    PermitRootLogin no
    Protocol 2
    AllowUser login guest
    AllowGroups admin
    # PasswordAuthentication no

## docker

https://www.docker.com/

    # usermod -aG docker $USER

then reboot

## DNS

https://www.cloudflare.com/

    1.1.1.1,1.0.0.1

or nextDNS: https://nextdns.io/

## flatpak

[https://flatpak.org](https://flatpak.org)

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## Gnome

### Extensions

remove systems extensions

    # rm -rf /usr/share/gnome-shell/extensions/

list:

- Launch new instance
- Burn my widow
- Clipboard Indicator
- GSConnect
- Compiz windows effect

## firefox

### Extensions

- UBO
- i still don't care about cookies
- Bitwarden
- floccus
