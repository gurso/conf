#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

debounce_time=${1:-5}
shift
last_exec=0

execute_command() {
    "$@"
    last_exec=$(date +%s)
}

main() {
    current_time=$(date +%s)
    time_since_last_exec=$((current_time - last_exec))

    if [ "$time_since_last_exec" -ge "$debounce_time" ]; then
        execute_command "$@"
        echo "Command executed: $@"
    else
        echo "Command debounced: $@"
    fi
}
main "$@"
