#!/usr/bin/env bash
set -e

local_branchs=`git branch --format="%(refname:short)"`
remote_branchs=`git branch -r --format="%(refname:short)"`

for l in $local_branchs
do
	sync=false
	for br in $remote_branchs
	do
		r="${br/origin\//}"
		if [ "$l" = "$r" ]
		then
			sync=true
			break
		fi
	done
	if [ $sync = false ]
	then
		read -p "Delete branch $l (y/N) ? " rm

		if [ "$rm" = "y" -o "$1" = "-y" ]
		then
			git branch -D "$l"
		fi
		echo ""
	fi
done

