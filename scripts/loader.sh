#!/usr/bin/env bash

i=1
sp="/-\|"
echo -n ' '

function loader() {
  printf "\b${sp:i++%${#sp}:1}"
	if [ -d /proc/$1 ]
	then
		sleep 0.3
		loader $1
	fi
}

loader $1
printf "\b"
