#!/usr/bin/env bash
set -o errexit -o pipefail
shopt -s dotglob

# Options

verbose=""
delete=""
# copy=""
force=""

while getopts "dvcf" opt; do
  case $opt in
    d) delete=true;;
    v) verbose=true ;;
    # c) copy=true ;;
    f) force=true ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done

shift $((OPTIND - 1))

rel="$(dirname $0)/../home"
conf=$(realpath $rel)
src_dir=${1:-$conf}
dest_dir=${2:-$HOME}

# Functions

v_print () {
		if [ "$verbose" ];then
				echo "$@"
		fi
}

safe_unlink() {
		if [ -L "$1" ] ;then
				unlink "$1"
				v_print "🧹 Unlink -> $1"
		elif [ -d "$1" ];then
			dlk=$0
			dlk="$dlk$($verbose && echo ' -v' || echo '')"
			dlk="$dlk$($force && echo ' -f' || echo '')"
			dlk="$dlk$($delete && echo ' -d' || echo '')"
			dlk="$dlk$($copy && echo ' -c' || echo '')"
			echo "DLK $dlk"
			$dlk $2 $1
		elif [ "$force" ];then
			rm -rf "$1"
			v_print " Remove -> $1"
		fi
}

# link_or_copy() {
# 		local src=$1
# 		local dest=$2
# 		if [ "$copy" ];then
# 				cp $src $dest
# 				v_print " $dest -> $src"
# 		else
# 				ln -s $src $dest
# 				v_print "🔗 $dest -> $src"
# 		fi
# }

safe_link() {
		local src=$1
		local dest=$2
		if [ "$delete" ];then
				safe_unlink $dest
		elif [ -s "$2" ];then

				local real_dest=$(readlink -f $dest)

				if [ "$real_dest" = "$src" ];then
						v_print "Link '$dest' already exist 😊"
				else
						answer="y"
						if [ ! $force ];then
							read -p "$2 exist, replace with link ? (y/N)" answer
						fi

						if [ "$answer" = "y" ];then
								if [ -L "$2" ];then
										safe_unlink $dest
								else
										rm -rf "$dest"
								fi
								ln -s $src $dest
								v_print "🔗 $dest => $src"
						fi
				fi
		else 
				ln -s $src $dest
				v_print "🔗 $dest => $src"
		fi
}

# Script

if [ -d "$src_dir" ] && [ -d "$dest_dir" ]
then
		list="$src_dir/*"
		for src in $list
		do
				base_name=$(basename $src)
				dest="$dest_dir/$base_name"
				v_print "Set $src to -> $dest"

				if [ "$delete" ];then
					safe_unlink $dest $src
				elif [ -d $src ];then
					v_print "Source $src is directory"
					if [ -d "$dest" ];then
						v_print "Dest $src is directory"
						real_path=$(readlink -f $dest)
						if [ $src = $real_path ]
						then
							v_print "Link '$dest' already exist 😊"
						else
							echo "$dest already exist, choose behavior"
							PS3="Enter a number: "
							select v in replace merge ignore
							do
								case $v in
									replace)
										rm -rf $dest
										safe_link $src $dest
										break
										;;
									ignore)
										break
										;;
									merge)
										$0 $src $dest
										break
										;;
									*)
										echo "Invalid option $REPLY"
										;;
								esac
							done
						fi
					else
						safe_link $src $dest
					fi
				elif [ -f "$src" ];then
					v_print "Source $src is file"
					safe_link $src $dest
				fi
			done
		else 
			echo "Source and destination should be directory" >&2 
			exit 1
fi

