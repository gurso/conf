#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

if [ ! -f $1 ]
then
	echo "File $1 not found."
	exit 1
fi

file_name=$(basename $1)
dir=$(dirname $1)
# abs_dir=$(dirname $(realpath $1))
d=$(date --iso-8601=seconds)
backup_name="backup-$d.$file_name"
backup_file="$dir/$backup_name"

cp $1 $backup_file
