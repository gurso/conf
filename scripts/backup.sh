#!/usr/bin/env bash

if [ -z $1 ]
then
	echo "Missing src folder to backup in arg"
	exit 1
fi

src=$1
dest=$2
: ${dest:="$PWD"}

current_date=$(date -I)

# rsync -avbn --backup-dir $2/$current_date --delete
rsync -av --delete $src $dest/$current_date
