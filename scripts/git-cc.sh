#!/usr/bin/env bash
set -e

ts=$(date +%s)
tmp_file="/tmp/convention-commit-tmp-file-uniq-$ts"

types=("fix" "hotfix" "feat" "refactor" "ci" "docs" "perf" "test" "config")
type="noexist"
scope=""
desc=""
body=""
icon=""
dry_run=""

function print_icon() {
	case "$1"
		in
		fix) echo "🐛";;
		feat) echo "✨";;
		hotfix) echo "🚑️";;
		refactor) echo "♻️";;
		ci) echo "👷";;
		config) echo "🔧";;
		docs) echo "📝";;
		perf) echo "⚡️";;
		test) echo "🧪";;
	esac
}

while getopts t:s:n opt
do 
	case "$opt"
		in
		t) type="$OPTARG";;
		s) scope="$OPTARG";;
		n) dry_run=true;;
	esac
done

function join() {
  local IFS="$1"
  shift
  echo "$*"
}

types_str=$(join "/" "${types[@]}")

	select t in ${types[@]}
	do 
		if [[ ! -z "$t" ]]
		then
			type=$t
			break
		fi
	done

icon=$(print_icon $type)

read -e -p "scope : " -i "$scope" scoped

if [ -n "$scoped" ]
then
	scoped="($scoped)"
fi

while [ -z "$desc" ]
do
	read -p "description : " desc
	if [ -z "$desc" ]
	then
		echo "Description is required !"
	fi
done

read -p "body : " body

echo -e "$icon $type$scoped: $desc\n\n$body" > $tmp_file
if [ -z $dry_run ]
then
	git commit -F $tmp_file
else
	echo "DRY RUN : "
	echo -e "$icon $type$scoped: $desc\n\n$body"
fi

