return {

	-- Plugins often use by other plugins
	"nvim-lua/popup.nvim",
	"nvim-lua/plenary.nvim",

	-- find and replace
	"nvim-pack/nvim-spectre",

	{
		"numToStr/Comment.nvim",
		opts = {},
		lazy = false,
	},
	{
		"kylechui/nvim-surround",
		config = function()
			require("nvim-surround").setup({})
		end,
	},
	{
		"m4xshen/autoclose.nvim",
		config = function()
			require("autoclose").setup()
		end,
	},
}
