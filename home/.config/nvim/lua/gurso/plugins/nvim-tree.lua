return {
	"nvim-tree/nvim-tree.lua",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	-- disable_netrw = true
	-- filters = {
	-- 	dotfiles = true,
	-- },
	config = function()
		require("nvim-tree").setup({
			view = {
				width = 50,
				side = "right"
			}
		})
		vim.api.nvim_set_keymap("n", "<leader>f", ":NvimTreeFindFile<cr>", { silent = true, noremap = true })
		vim.api.nvim_set_keymap("n", "<leader>o", ":NvimTreeToggle<cr>", { silent = true, noremap = true })

		-- local function open_tab_silent(node)
		-- 	local api = require("nvim-tree.api")
		-- 	api.node.open.tab(node)
		-- 	vim.cmd.tabprev()
		-- end
		--
		-- vim.keymap.set("n", "T", open_tab_silent)
	end,
}
