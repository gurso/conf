-- return {
-- 	"nosduco/remote-sshfs.nvim",
-- 	dependencies = { "nvim-telescope/telescope.nvim" },
-- config = function()
-- 	require("remote-sshfs").setup({})
-- 	require("telescope").load_extension("remote-sshfs")
-- 	local api = require("remote-sshfs.api")
-- 	vim.keymap.set("n", "<leader>rc", api.connect, {})
-- 	vim.keymap.set("n", "<leader>rd", api.disconnect, {})
--
-- 	-- local builtin = require("telescope.builtin")
-- 	-- local connections = require("remote-sshfs.connections")
-- 	-- vim.keymap.set("n", "<leader>sf", function()
-- 	-- 	if connections.is_connected then
-- 	-- 		api.find_files()
-- 	-- 	else
-- 	-- 		builtin.find_files()
-- 	-- 	end
-- 	-- end, {})
-- 	-- vim.keymap.set("n", "<leader>sg", function()
-- 	-- 	if connections.is_connected then
-- 	-- 		api.live_grep()
-- 	-- 	else
-- 	-- 		builtin.live_grep()
-- 	-- 	end
-- 	-- end, {})
-- end,
-- }

return {
	"DanielWeidinger/nvim-sshfs",
	config = function()
		require("sshfs").setup({
			mnt_base_dir = vim.fn.expand("$HOME") .. "/mnt",
			width = 0.6, -- host window width
			height = 0.5, -- host window height
			connection_icon = "✓", -- icon for connection indication
		})
	end,
}
