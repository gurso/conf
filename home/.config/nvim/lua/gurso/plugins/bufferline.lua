return {
	"akinsho/bufferline.nvim",
	dependencies = "nvim-tree/nvim-web-devicons",
	event = "VeryLazy",
	config = function()
		require("bufferline").setup({
			options = {
				numbers = "none", -- "ordinal" | ...
				offsets = {
					{ filetype = "NvimTree", text = "", padding = 1 },
				},
			},
		})
	end,
}
