return {
	"stevearc/conform.nvim",
	event = "VeryLazy",
	opts = {
		notify_on_error = false,
		format_on_save = {
			timeout_ms = 1000,
			lsp_fallback = true,
		},
		formatters_by_ft = {
			lua = { "stylua" },
			typescript = { "prettier" },
			javascript = { "prettier" },
			html = { "prettier" },
			vue = { "prettier" },
			css = { "prettier" },
		},
	},
}
