return {
	"williamboman/mason.nvim",
	event = "VeryLazy",
	dependencies = { "williamboman/mason-lspconfig.nvim" },
	config = function()
		local mason = require("mason")
		local masonLspConfig = require("mason-lspconfig")

		mason.setup({
			ensure_installed = { "prettier", "stylua" },
			ui = {
				border = "rounded",
			},
		})
		masonLspConfig.setup({
			ensure_installed = {
				"lua_ls",
				"rust_analyzer",
				"eslint",
				"ts_ls",
				"volar",
				"html",
				"cssls",
				"tailwindcss",
			},
			automatic_installation = true,
		})
	end,
}
