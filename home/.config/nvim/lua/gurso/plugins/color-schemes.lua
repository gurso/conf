-- Colorschemes
-- return {
-- 	"folke/tokyonight.nvim",
-- 	lazy = false,
-- 	priority = 1000,
-- 	opts = {},
-- 	config = function()
-- 		-- load the colorscheme here
-- 		vim.cmd([[colorscheme tokyonight]])
-- 	end,
-- }
return {
	"catppuccin/nvim",
	lazy = false,
	priority = 1000,
	opts = {},
	config = function()
		-- load the colorscheme here
		vim.cmd([[colorscheme catppuccin]])
	end,
}
