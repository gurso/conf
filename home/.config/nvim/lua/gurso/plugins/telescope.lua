return {
	{
		"nvim-telescope/telescope.nvim",
		event = "VeryLazy",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope-symbols.nvim",
			"nvim-telescope/telescope-ui-select.nvim",
			"nvim-telescope/telescope-media-files.nvim",
		},
		config = function()
			local telescope = require("telescope")
			local builtin = require("telescope.builtin")
			vim.keymap.set("n", "<leader>ss", builtin.builtin, { desc = "[S]earch [S]elect Telescope" })
			vim.keymap.set("n", "<leader>sh", builtin.help_tags, { desc = "[S]earch [H]elp" })
			vim.keymap.set("n", "<leader>sk", builtin.keymaps, { desc = "[S]earch [K]eymaps" })
			vim.keymap.set("n", "<leader>sf", builtin.find_files, { desc = "[S]earch [F]iles" })
			vim.keymap.set("n", "<leader>s.", builtin.oldfiles, { desc = "[S]earch Recent Files ('.' for repeat)" })
			vim.keymap.set("n", "<leader>sb", builtin.buffers, { desc = "[S]earch [B]uffers" })
			vim.keymap.set("n", "<leader>sg", builtin.live_grep, { desc = "[S]earch by [G]rep" })
			vim.keymap.set("n", "<leader>sd", builtin.diagnostics, { desc = "[S]earch [D]iagnostics" })

			-- LSP
			vim.keymap.set("n", "<leader>sr", builtin.lsp_references, { desc = "[S]earch [R]eferences" })
			vim.keymap.set("n", "<leader>ds", builtin.lsp_document_symbols, { desc = "[D]ocument [S]ymbols" })
			vim.keymap.set("n", "<leader>ws", builtin.lsp_dynamic_workspace_symbols, { desc = "[W]orkspace [S]ymbols" })

			vim.keymap.set("n", "<leader>se", builtin.symbols, { desc = "Looking for emoji" })
			vim.keymap.set(
				"n",
				"<leader>mf",
				telescope.extensions.media_files.media_files,
				{ desc = "[M]edia [F]iles" }
			)
			-- GIT
			-- vim.keymap.set("n", "<leader>sf", builtin.git_files, {})
			vim.keymap.set("n", "<leader>st", builtin.git_status, {})
			vim.keymap.set("n", "<leader>br", builtin.git_branches, {})

			telescope.setup({
				extensions = {
					["ui-select"] = {
						require("telescope.themes").get_dropdown(),
					},
					media_files = {
						-- filetypes whitelist
						filetypes = { "png", "webp", "jpg", "jpeg", "svg" },
					},
				},
			})

			telescope.load_extension("media_files")
			telescope.load_extension("ui-select")
		end,
	},
	-- {
	-- 	"nvim-telescope/telescope-file-browser.nvim",
	-- 	dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
	--
	-- 	config = function()
	-- 		local telescope = require("telescope")
	-- 		telescope.setup({
	-- 			extensions = {
	-- 				file_browser = {
	-- 					theme = "ivy",
	-- 					-- disables netrw and use telescope-file-browser in its place
	-- 					hijack_netrw = true,
	-- 				},
	-- 			},
	-- 		})
	-- 		vim.keymap.set(
	-- 			"n",
	-- 			"<leader>fb",
	-- 			telescope.extensions.file_browser.file_browser,
	-- 			{ desc = "[F]ile [B]rowser" }
	-- 		)
	-- 		telescope.load_extension("file_browser")
	-- 	end,
	-- },
}
