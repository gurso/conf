return {
	"L3MON4D3/LuaSnip",
	event = "InsertEnter",
	config = function()
		-- Doc to writte snippets in lua : https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md
		-- article : https://www.ejmastnak.com/tutorials/vim-latex/luasnip/
		--
		-- command to see snippets :LuaSnipListAvailable
		local ls = require("luasnip")

		require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/lua/gurso/snippets/" })
		-- require("luasnip.loaders.from_lua").load({ paths = p.abs("../snippets/") })

		vim.keymap.set({ "i", "s" }, "<c-k>", function()
			if ls.expand_or_jumpable() then
				ls.expand_or_jump()
			end
		end, { silent = false })
	end,
}
