local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node

return {
	s({
		trig = "snip",
		dscr = "init new snippet file",
	}, {
		t({
			'local ls = require("luasnip")',
			"local s = ls.snippet",
			"local t = ls.text_node",
			"",
			"return {",
			"	s({",
			'		trig = ""',
			"	}, {",
			"		t({",
			"			",
			"		})",
			"	})",
			"}",
		}),
	}),
	s({
		trig = "snp",
		dscr = "create snippet obj",
	}, {
		t({
			"	s({",
			'		trig = ""',
			"	}, {",
			"		t({",
			"			",
			"		})",
			"	})",
		}),
	}),
}
