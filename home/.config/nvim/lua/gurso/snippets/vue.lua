local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

local ts = require("gurso.snippets.typescript")

local vue = {
	s({ trig = "sfc" }, {
		t({
			'<script setup lang="ts">',
			"</script>",
			"",
			"<template>",
			"\t",
		}),
		i(1),
		t({ "", "</template>" }),
	}),
	ts[1],
	ts[2],
	ts[3],
	ts[4],
}

-- TODO: merge and return ts + vue
-- return merge(vue, ts)
return vue
