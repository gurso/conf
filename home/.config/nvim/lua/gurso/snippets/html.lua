local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

return {
	s({
		trig = "html5",
	}, {
		t({
			"<!DOCTYPE html>",
			"<html>",
			"<head>",
			'\t<meta charset="utf-8" />',
			'\t<meta name="viewport" content="width=device-with, initial-scale=1.0" />',
			"\t<title>",
		}),
		i(1),
		t({
			"</title>",
			"</head>",
			"<body>",
		}),
		i(2),
		t({
			"</body>",
			"</html>",
		}),
	}),
}
