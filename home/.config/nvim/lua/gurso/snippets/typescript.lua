local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

return {
	s({
		trig = "lg",
	}, {
		t({
			"console.log(",
		}),
		i(1),
		t({ ")" }),
	}),

	s({ trig = "c" }, {
		t({ "console." }),
	}),

	s({
		trig = "fn",
	}, {
		t({
			"function ",
		}),
		i(1),
		t({ "(" }),
		i(2),
		t({ ") {" }),
		i(3),
		t({ "}" }),
	}),

	s({
		trig = "afn",
	}, {
		t({
			"async function ",
		}),
		i(1),
		t({ "(" }),
		i(2),
		t({ ") {" }),
		i(3),
		t({ "}" }),
	}),

	s({
		trig = "efn",
	}, {
		t({
			"export function ",
		}),
		i(1),
		t({ "(" }),
		i(2),
		t({ ") {" }),
		i(3),
		t({ "}" }),
	}),

	s({
		trig = "eafn",
	}, {
		t({
			"export async function ",
		}),
		i(1),
		t({ "(" }),
		i(2),
		t({ ") {" }),
		i(3),
		t({ "}" }),
	}),

}
