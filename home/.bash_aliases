### Usuals

alias sudo="sudo "
alias c=clear
alias l="ls --color"
alias la="ls -Alh --color"
alias j=jobs
alias z="zellij"
alias lzg="lazygit"
alias lzd="lazydocker"
alias xc="xclip -selection clipboard"
alias tgz="tar -czvvf"
alias ugz="tar -xzvvf"
alias up="pacman -Syu"

function saila() {
	# not work with function, use "type" command
	alias | grep $1 | cut -d ' ' -f 2-
}

### Docker

alias dps="docker ps"
alias dpa="docker ps -a"
alias dcu="docker compose up"
alias dcs="docker compose stop"
alias dcd="docker compose down"

function dex {
	docker exec -it $(docker ps -f name=$1 -q) ${2:-bash}
}
function dlg {
	docker logs $(docker ps -f name=$1 -q)
}
function dka {
	docker stop  $(docker ps --all -q)
	docker rm $(docker ps --all -q)
}

### neovim

function v {
	nvim ${1:-$PWD}
}
alias view="nvim -R"
alias nvconf="nvim $HOME/.config/nvim/"

### yazi

function yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		builtin cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

### vite

function vit() {
	name=${1:-"app"}
	npm create vite@latest $name -- --template vue-ts
}

